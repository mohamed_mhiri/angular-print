import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { TemplateService } from "./template.service";
import { Template } from './models/template'
import 'fabric';
declare const fabric: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  AllTemplates: Array<Template> = [];
  activeTemplate: Array<any> = [];
  currentTemplateIndex = 0;
  constructor(private templateService: TemplateService,
    public ref: ChangeDetectorRef) {

  }
  //this is the array where we will put the list of template preview images
  slides = [
    { img: "http://placehold.it/350x800/000000" }
  ];
  slideConfig = { "slidesToShow": 4, "slidesToScroll": 1 };

  addSlide() {
    this.slides.push({ img: "http://placehold.it/350x800/777777" })
  }

  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }

  ngOnInit() {
    this.templateService.getTemplates()
      .subscribe((templates: Array<any>) => {
        if (templates.length > 0) {
          templates.forEach((template) => {
            var temp = new Template(template.allCanvas, template._id);
            this.AllTemplates.push(temp);
            this.addSlide();
          })
          this.activeTemplate = this.AllTemplates[0].allCanvas;
          this.currentTemplateIndex = 0;
          this.removeSlide();
        }
      })
  }


  addTemplate() {
    if (!confirm("Are you sure to quit the current template ?")) {
      return;
    } else {
      const emptyTemplate = [
        {
          json: '{}',
          width: fabric.util.parseUnit(`120mm`),
          height: fabric.util.parseUnit(`150mm`),
          backgroundColor: '#ffffff'
        }
      ];
      var newTemplate = new Template(emptyTemplate);
      this.templateService.saveTemplate(newTemplate)
        .subscribe((template: any) => {
          var returnedTemplate = new Template(template.allCanvas, template._id);
          this.AllTemplates.push(returnedTemplate);
          this.activeTemplate = returnedTemplate.allCanvas;
          this.currentTemplateIndex++;
          this.addSlide();
        })

    }
  }

  updateTemplate(template) {
    if (this.AllTemplates[this.currentTemplateIndex]) {
      this.AllTemplates[this.currentTemplateIndex].allCanvas = template;
      let temp = new Template(template, this.AllTemplates[this.currentTemplateIndex].id);
      this.templateService.saveTemplate(temp)
        .subscribe((template: any) => {
          //this.AllTemplates[this.currentTemplateIndex]=new Template(template.allCanvas,template._id);
        })
    } else {
      let temp1 = new Template(template);
      this.templateService.saveTemplate(temp1)
        .subscribe((template: any) => {
          let res = new Template(template.allCanvas, template._id);
          this.AllTemplates[this.currentTemplateIndex] = res;
        })
    }
  }

  loadTemplate(i: number) {
    if (!confirm("Are you sure to quit the current template ?")) {
      return;
    } else {
      if (!this.AllTemplates[this.currentTemplateIndex]) { this.removeSlide() }
      if (this.AllTemplates[i]) {
        this.activeTemplate = this.AllTemplates[i].allCanvas;
        this.currentTemplateIndex = i;
      }
    }

  }
  deleteTemplate() {
    if (this.AllTemplates.length == 0) {
      this.activeTemplate = [{
        json: '{}',
        width: fabric.util.parseUnit(`120mm`),
        height: fabric.util.parseUnit(`150mm`),
        backgroundColor: '#ffffff'
      }];

    } else if (this.AllTemplates.length == 1) {
      //let template=
      this.templateService.deleteTemplate(this.AllTemplates[this.currentTemplateIndex].id)
        .subscribe(() => {
          this.AllTemplates = [];
          let template = [{
            json: '{}',
            width: fabric.util.parseUnit(`120mm`),
            height: fabric.util.parseUnit(`150mm`),
            backgroundColor: '#ffffff'
          }];
          this.activeTemplate = template;
        })


    } else if (this.currentTemplateIndex == this.AllTemplates.length - 1) {
      if (this.AllTemplates[this.currentTemplateIndex].id) {
        this.templateService.deleteTemplate(this.AllTemplates[this.currentTemplateIndex].id)
          .subscribe(() => {
            this.AllTemplates.splice(this.currentTemplateIndex, 1);
            this.activeTemplate = this.AllTemplates[this.currentTemplateIndex - 1].allCanvas;
            this.currentTemplateIndex--;
            this.removeSlide();
          });
      }

    } else {
      if (this.AllTemplates[this.currentTemplateIndex].id) {
        this.templateService.deleteTemplate(this.AllTemplates[this.currentTemplateIndex].id)
          .subscribe(() => {
            this.AllTemplates.splice(this.currentTemplateIndex, 1);
            this.activeTemplate = this.AllTemplates[this.currentTemplateIndex].allCanvas;
            this.removeSlide();
          });
      }

    }


  }
  log() {
    console.log(this.AllTemplates);
  }

  afterChange(event) {
    console.log(event);    
  }

}
