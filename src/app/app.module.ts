import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';

import { AppComponent } from './app.component';
import { SlickModule } from 'ngx-slick';
import { HeaderComponent } from './header/header.component';
import { TemplateComponent } from './template/template.component';
import {TemplateService} from "./template.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TemplateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ColorPickerModule,
    HttpClientModule,
    SlickModule.forRoot()

  ],
  providers: [TemplateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
