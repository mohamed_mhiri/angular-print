import { Injectable } from '@angular/core';
import {HttpClient ,HttpHeaders} from "@angular/common/http";

@Injectable()
export class TemplateService {

  constructor(private http:HttpClient) { }

  getTemplates(){
    return this.http
      .get('http://51.38.177.155:3000/api/template')
  }

  saveTemplate(template){
    const templateJson=JSON.stringify(template);
    console.log(templateJson);
    if (!template.id){
      return this.http
        .post('http://51.38.177.155:3000/api/template',templateJson,
          {headers: new HttpHeaders().set('Content-Type', 'application/json')})
    }else {
     return this.http
        .put('http://51.38.177.155:3000/api/template/'+template.id,templateJson,
          { headers: new HttpHeaders().set('Content-Type', 'application/json')})
    }
  }

  deleteTemplate(id){
    return this.http
      .delete('http://51.38.177.155:3000/api/template/'+id)
  }



}
