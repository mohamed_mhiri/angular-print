FROM nginx
RUN apt-get update
RUN apt-get -y install git

RUN git clone https://github.com/vmware/clarity-seed.git
RUN apt-get -y install build-essential
RUN apt-get -y install curl
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get -y install nodejs
RUN npm install -g @angular/cli

#RUN npm install
#RUN npm run build

#COPY dist /usr/share/nginx/html
EXPOSE 5000